<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('main');
});

/*Route::get('customer/{id}', 'CustomerController@customer');

Route::get('customer_name', function(){
    $customer = App\Models\Customer::where('name', '=', 'Tony')->first();
    echo $customer->id;
});

Route::get('orders', function(){
    $orders = App\Models\Order::all();
    foreach($orders as $order){
        echo $order->name . ' belongs to ' . $order->customer->name . '<br>';
    }
});*/


//Route::get('/home', 'HomeController@index');

//Route::get('/test', 'TestController@index');

Auth::routes();

Route::get('main',['as' => 'main', 'uses' => 'MainController@index']);
Route::get('main/find',['as' => 'main/find', 'uses' => 'MainController@find']);
Route::post('main/find',['as' => 'main/find', 'uses' => 'MainController@find']);
Route::get('main/view-advert/{id}',['as' => 'main/view-advert', 'uses' => 'MainController@viewAdvert'])->where('id', '[0-9]+');

Route::post('main/message',['as' => 'main/message', 'uses' => 'SubscribeController@message']);
Route::get('contact',['as' => 'contact', 'uses' => 'SubscribeController@contact']);
Route::post('contact',['as' => 'contact', 'uses' => 'SubscribeController@contact']);
Route::post('notify', ['as' => 'notify', 'uses' => 'SubscribeController@notify']);
Route::get('about',['as' => 'about', 'uses' => 'MainController@about']);

Route::post('login', ['as' => 'login', 'uses' => 'Auth\LoginController@login' ]);
Route::get('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);

//Route::resource('admin/posts', 'Admin\\PostsController');

Route::group(['as' => 'cabinet/adverts'] ,function () {
    Route::get('cabinet/adverts/images', 'AdvertController@images')->name('/images');
    Route::post('cabinet/adverts/uploadGeneral', 'AdvertController@uploadGeneral');
    Route::post('cabinet/adverts/uploadImages', 'AdvertController@uploadImages');
    Route::post('cabinet/adverts/deleteImages', 'AdvertController@deleteImages');
});

Route::resource('cabinet/admins', 'AdminController');

Route::resource('cabinet/adverts', 'AdvertController');

Route::get('/404', ['as' => 'error.404']);
