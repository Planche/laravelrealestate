<?php

namespace App\Providers;

use App\Contracts\AdvertContract;
use App\Contracts\MockTestContract;
use App\Repositories\AdvertRepository;
use App\Repositories\MockTestRepo;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            AdvertContract::class,
            AdvertRepository::class
        );

        $this->app->bind(
            MockTestContract::class,
            MockTestRepo::class
        );

       $this->app->bind(
            'App\Contracts\TestInterface',
            'App\Myclass\Test'
        );
        $this->app->bind(
            'App\Contracts\TestInterface',
            'App\Myclass\AnotherTest'
        );
    }
}
