<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Myclass\Test;

class TestProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Test::class, function(){
            return new Test('Tess');
        });
    }
}
