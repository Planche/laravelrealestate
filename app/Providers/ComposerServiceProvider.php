<?php

namespace App\Providers;


use App\Models\Advert;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['realestate.find','realestate.view-advert'], function ($view)
        {
            /** @var  $view Advert */
            $view->with('sidebar', Advert::whereHot(1)->take(4)->get())
                 ->with('sort');
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
