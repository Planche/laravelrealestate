<?php

namespace App\Http\Controllers;

use App\Models\Subscriber;
use App\Models\User;
use App\Notifications\Contact;
use App\Notifications\Message;
use App\Notifications\NotifyMe;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{
    /**
     * Subscribe validation and Send Email and save email to database.
     *
     * @param Request $request
     * @return \Response json
     */
    public function notify(Request $request)
    {
        if($request->ajax()){

            $rules = ['email' => 'required|email|unique:subscribers'];
            $customMessages = ['unique' => 'This E-mail is notified.'];

            $this->validate($request, $rules, $customMessages);

            $requestData = $request->all();
            $model = Subscriber::create($requestData);

            $user = User::first();
            try {
                $user->notify(new NotifyMe($model));
                return response()->json([
                    'success' => true,
                    'message' => 'You have subscribed for Notifications'
                ], 200);
            }
            catch(\Exception $e){
                return response()->json([
                    'success' => false,
                    'errors' => 'Unable to send Message',
                    'message' => utf8_encode($e->getMessage())
                ], 500);
            }
        }
        else {
            return response()->json([
                'success' => false,
                'errors' => 'Not Ajax Call'
            ], 422);
        }
    }

    /**
     * Message Validation and Send Message by Email.
     *
     * @param Request $request
     * @return \Response json
     */
    public function message(Request $request)
    {
        if($request->ajax()){

            $rules = ['mail' => 'required|email','message' => 'required'];

            $this->validate($request, $rules);

            $requestData = $request->all();

            $user = User::first();
            try {
                $user->notify(new Message($requestData));
                return response()->json([
                    'success' => true,
                    'message' => 'Message sent successfully'
                ], 200);
            }
            catch(\Exception $e){
                return response()->json([
                    'success' => false,
                    'errors' => 'Unable to send Message',
                    'message' => utf8_encode($e->getMessage())
                ], 500);
            }
        }
        else {
            return response()->json([
                'success' => false,
                'errors' => 'Not Ajax Call'
            ], 422);
        }
    }

    /**
     * Message Validation and Send Message by Email.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function contact(Request $request)
    {
        if($request->isMethod('post')){

            $rules = ['fullname' => 'required','emailaddress' => 'required|email','message' => 'required'];

            $this->validate($request, $rules);

            $requestData = $request->all();

            $user = User::first();
            try {
                $user->notify(new Contact($requestData));
                \Session::flash('success_message', 'Message has been Sent!');
                return redirect('contact');
            }
            catch(\Exception $e){
                \Session::flash('error_message', 'Unable to sand message! Internal server error');
                return redirect('contact');
            }
        }

        return view('realestate/contact');
    }
}
