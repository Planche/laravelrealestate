<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdvertValidateRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\AdvertContract;
use App\Models\Advert;
use Intervention\Image\Facades\Image;
use Session;

class AdvertController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  AdvertContract $advertRepo
     * @return \Illuminate\View\View
     */
    public function index(Request $request, AdvertContract $advertRepo)
    {

        Session::flash('idAdvert', 'Welcome');
        $adverts = $advertRepo->getAdverts($request);

        $columns = \Schema::getColumnListing('adverts');
        return view('cabinet.adverts.index', compact('adverts','columns'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cabinet.adverts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdvertValidateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(AdvertValidateRequest $request)
    {

        $requestData = $request->all();

        $requestData['fk_agent'] = \Auth::id();

        $model = Advert::create($requestData);

        Session::flash('flash_message', 'Advert added!');
        Session::put('idAdvert', $model->getAttribute('id'));
        return redirect('cabinet/adverts/images');
    }

    /**
     * Show form for image upload.
     *
     * @return \Illuminate\View\View
     */
    public function images()
    {
        if($idAdvert = Session::get('idAdvert'))
        {
            $image = $images = null;
            $model = Advert::findorfail($idAdvert);
            if($general_image = $model->general_image)
            {
                // Path for General Image

                $imageGeneral = storage_path().'/app/public/uploads/'. $idAdvert .'/general/' . $general_image;
                if(\File::exists($imageGeneral)){
                    $image = '/storage/uploads/'. $idAdvert .'/general/' . $general_image;
                } else {
                    $image = null;
                }

                //Path for Additional Images
                $path = storage_path().'/app/public/uploads/'. $idAdvert;
                if(\File::exists($path))
                {
                    $imagePaths = \File::files($path);
                    foreach ($imagePaths as $row) {

                        $images[] = '/storage/uploads/'. $idAdvert . '/' . basename($row);
                    }
                } else {$images = null;}


                return view('cabinet.adverts.images', compact('image', 'images'));
            }
            else {
                return view('cabinet.adverts.images', compact('image', 'images'));
            }
        }
        else {
            //return view('cabinet.adverts.images');
            return response()->view('404');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function uploadGeneral(Request $request)
    {
        $rules = ['file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'];
        if($request->ajax())
        {
            $file = $request->file('file');

            $this->validate($request, $rules);

            $ext = $file->getClientOriginalExtension();
            $fileName = 'general.'.$ext;


            if($file->storeAs('public/uploads/'. Session::get('idAdvert') .'/general',$fileName)){

                $model = Advert::find(Session::get('idAdvert'));
                $model->general_image = $fileName;
                $model->save();
                //Session::flash('flash_message', 'General Image Added!');

                $small = Image::make($request->file('file')->getRealPath());
                $small->resize(1000,644);
                $small->save(storage_path().'/app/public/uploads/'.Session::get('idAdvert').'/general/small_'.$fileName);

                return response()->json(['success' => true],200);
            }
            else {
                // sending back with error message.
                return response()->json(['success' => false, 'error' => 'Unable to save file'],422);
            }
        }
        else {
            return response()->json(['success' => false, 'error' => 'Not Ajax Call'],422);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Response
     */
    public function uploadImages(Request $request)
    {
        $rules = ['images' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'];
        if($request->ajax())
        {
            $file = $request->file('images');

            $this->validate($request, $rules);

            $fileName = $file->getClientOriginalName();

            if ($file->storeAs('public/uploads/' . Session::get('idAdvert'), $fileName)) {
                return response()->json(['success' => true],200);
            } else {
                // sending back with error message.
                return response()->json(['success' => false, 'error' => 'Unable to save file'],422);
            }
        }
        else {
            return response()->json(['success' => false, 'error' => 'Not Ajax Call'],422);
        }
    }
    /**
     * Delete image from storage/uploads.
     *
     * @param \Illuminate\Http\Request  $request
     *
     * @return \Response
     */
    public function deleteImages(Request $request)
    {
        if($request->ajax())
        {
            $check = false;

            if($request->request->get('key')){
                $imageUrl = $request->request->get('key');
                $imagePath = ltrim($imageUrl, '/');
            } else {
                $imagePath = false;
            }

            \File::exists($imagePath) ? $check = \File::delete($imagePath) :  false;

            if($check) {
                return response()->json(['success' => true,], 200);
            } else{
                return response()->json(['success' => false, 'error' => 'File not exist' ],422);
            }


        }
        else {
            return response()->json(['success' => false, 'error' => 'Not Ajax Call'],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $advert = Advert::findOrFail($id);

        $columns = \Schema::getColumnListing('adverts');
        return view('cabinet.adverts.show', compact('advert','columns'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $advert = Advert::findOrFail($id);

        return view('cabinet.adverts.edit', compact('advert'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param AdvertValidateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, AdvertValidateRequest $request)
    {

        $requestData = $request->all();

        $advert = Advert::findOrFail($id);
        $advert->update($requestData);

        Session::flash('flash_message', 'Advert updated!');
        Session::put('idAdvert', $advert->getAttribute('id'));
        return redirect('cabinet/adverts/images');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Advert::destroy($id);

        Session::flash('flash_message', 'Advert deleted!');

        return redirect('cabinet/adverts');
    }
}
