<?php

namespace App\Http\Controllers;

use App\Contracts\TestInterface;
use App\Models\Advert;
use App\Models\User;
use App\Myclass\AnotherTest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    /**
     * Return front page content.
     *
     * @param Advert $advert
     *
     * @return \Illuminate\View\View
     */
    public function index(Advert $advert)
    {
        //more changes
        // Main slider
        $adverts = $advert->all()->sortByDesc('id')->take(5);
        $adverts_count = $adverts->count();

        // Featured adverts(carousel)
        $featured = $advert->all()->take(15);

        // Recommended slider content
        $recommend = $advert->all()->where('recommend',1)->sortByDesc('id')->take(5);
        $recommend_count = $recommend->count();

        /*$e = Advert::select('*')
            ->orderByRaw("ABS(price - :value), ABS(parking - :another)", ['value' => 40000, 'another' => 9])->take(1)
            ->get();*/
        return view('realestate/index',compact('adverts','adverts_count','featured','recommend','recommend_count'));
    }

    /**
     * Return search results.
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function find(Request $request)
    {
        //avelcrinq
        $perPage = 3;
        $query = Advert::query();

        if($request->ajax())
        {
            if($request->session()->has('advert'))
            {
                $searchRow = $request->session()->get('advert');

                $query = $query->where(function ($q) use ($searchRow) {
                    /** @var  $q Builder */
                    $q->where('address', 'LIKE', "%$searchRow%")
                        ->orWhere('description', 'LIKE', "%$searchRow%");
                });
            }

            if($request->session()->has('price'))
            {
                $prices = explode("-",$request->session()->get('price'));
                if(isset($prices[0]) && isset($prices[1])) {
                    $query = $query->whereBetween('price',[$prices[0],$prices[1]]);
                }
                else {
                    $query = $query->where('price','>=',$prices[0]);
                }
            }

            if($request->session()->has('apartment')) {
                $query = $query->where('type','=',$request->session()->get('apartment'));
            }

            if($request->get('sortByPrice')) {
                $getQuery = $query->orderBy('price',$request->get('sortByPrice'))->get();
            }
            else {
                $getQuery = $query->get();
            }

            // Array and Collection Pagination
            $adverts = Advert::paginateArray($getQuery,$perPage,$request->get('page'),['path' => $request->root().'/main/find']);

            return view('includes.ajax-refresh-data', compact('adverts'));
        }
        else
        {
            $request->flash();
            //DB::connection()->enableQueryLog();
            $request->session()->forget(['advert', 'price', 'apartment']);

            if (!empty($request->get('advert'))) {
                $request->session()->put('advert', $request->get('advert'));
                $searchRow = $request->get('advert');

                $query = $query->where(function ($q) use ($searchRow) {
                    /** @var  $q Builder */
                    $q->where('address', 'LIKE', "%$searchRow%")
                        ->orWhere('description', 'LIKE', "%$searchRow%");
                });
            }

            if (!is_null($request->get('price'))) {
                $request->session()->put('price', $request->get('price'));
                $prices = explode("-", $request->get('price'));
                if (isset($prices[0]) && isset($prices[1])) {
                    $query = $query->whereBetween('price', [$prices[0], $prices[1]]);
                } else {
                    $query = $query->where('price', '>=', $prices[0]);
                }
            }

            if (!is_null($request->get('apartment'))) {
                $request->session()->put('apartment', $request->get('apartment'));
                $query = $query->where('type', '=', $request->get('apartment'));
            }

            $getQuery = $query->get();

            // Array and Collection Pagination
            $adverts = Advert::paginateArray($getQuery, $perPage, $request->get('page'), ['path' => $request->url()]);

            // Count all adverts
            $advertsCount = $getQuery->count();

            //dd(DB::getQueryLog());

            return view('realestate/find', compact('adverts', 'advertsCount'));
        }
    }

    /**
     * Return search results and sort by price.
     *
     * @param Request $request
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function viewAdvert(Request $request, $id)
    {
        //eli avelacav
        $advert = Advert::findorfail($id);
        $images = null;
        //Path for Additional Images
        $path = storage_path().'/app/public/uploads/'. $id;
        if(\File::exists($path))
        {
            $imagePaths = \File::files($path);
            foreach ($imagePaths as $row) {

                $images[] = '/storage/uploads/'. $id . '/' . basename($row);
            }
        } else {
            $images = null;
        }

        return view('realestate/view-advert', compact('advert','images'));
    }

    /**
     * Return search results and sort by price.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function about()
    {
        return view('realestate/about');
    }

}
