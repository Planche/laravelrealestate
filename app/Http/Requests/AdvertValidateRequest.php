<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdvertValidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price' => 'required',
            'address' => 'required|max:255',
            'bedroom' => 'required|integer',
            'livingroom' => 'required|integer',
            'parking' => 'required|integer',
            'kitchen' => 'required|integer',
            'description' => 'required|string',
            'hot' => 'required',
            'sold' => 'required',
            'recommend' => 'required',
        ];
    }
}
