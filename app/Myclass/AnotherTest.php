<?php

namespace App\Myclass;

use App\Contracts\TestInterface;

class AnotherTest implements TestInterface
{
    public static $variable;

    private $priv = 1;

    function __construct()
    {
        static::$variable = 1;
    }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public function testFunc()
    {
        return 'another test';
    }

    public function retStaticVar()
    {
        static $index = 0;
        return $index++;
    }

    public function setStaticVar($var)
    {
        static::$variable += $var;
    }
}