<?php
namespace App\Myclass;

use App\Contracts\TestInterface;

class Test implements TestInterface
{
    use teeest;
    private $name;
    public function __construct($name) {
        $this->name=$name;

    }
    public function say(){
        return "Hello ".$this->name;
    }

    public function testFunc()
    {
        return 'test';
    }

}

trait teeest
{
    public function myFirstTrait()
    {
        return "bla trait";
    }
}

//new func