<?php

namespace App\Repositories;


use App\Contracts\AdvertContract;
use App\Contracts\MockTestContract;
use App\Models\Advert;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class MockTestRepo implements MockTestContract
{
    public function getAllAdverts(Request $request)
    {
        $fk_agent = auth()->id();
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            if(\Auth::id() == 1) {
                $adverts = Advert::where('address', 'LIKE', "%$keyword%")
                    ->orWhere('bedroom', 'LIKE', "%$keyword%")
                    ->orWhere('livingroom', 'LIKE', "%$keyword%")
                    ->orWhere('parking', 'LIKE', "%$keyword%")
                    ->orWhere('kitchen', 'LIKE', "%$keyword%")
                    ->orWhere('general_image', 'LIKE', "%$keyword%")
                    ->orWhere('description', 'LIKE', "%$keyword%")
                    ->orWhere('location', 'LIKE', "%$keyword%")
                    ->orWhere('hot', 'LIKE', "%$keyword%")
                    ->orWhere('sold', 'LIKE', "%$keyword%")
                    ->orWhere('type', 'LIKE', "%$keyword%")
                    ->orWhere('recommend', 'LIKE', "%$keyword%")

                    ->paginate($perPage);
            }
            else {
                $adverts = Advert::where(function ($q) use ($keyword) {
                    /** @var  $q Builder */
                    $q->where('price', 'LIKE', "%$keyword%")
                        ->orWhere('address', 'LIKE', "%$keyword%")
                        ->orWhere('bedroom', 'LIKE', "%$keyword%")
                        ->orWhere('livingroom', 'LIKE', "%$keyword%")
                        ->orWhere('parking', 'LIKE', "%$keyword%")
                        ->orWhere('kitchen', 'LIKE', "%$keyword%")
                        ->orWhere('general_image', 'LIKE', "%$keyword%")
                        ->orWhere('description', 'LIKE', "%$keyword%")
                        ->orWhere('location', 'LIKE', "%$keyword%")
                        ->orWhere('hot', 'LIKE', "%$keyword%")
                        ->orWhere('sold', 'LIKE', "%$keyword%")
                        ->orWhere('type', 'LIKE', "%$keyword%")
                        ->orWhere('recommend', 'LIKE', "%$keyword%");
                })
                    ->where('fk_agent', '=', $fk_agent)
                    ->paginate($perPage);
            }
        } else {
            if(\Auth::id() == 1){
                $adverts = Advert::paginate($perPage);
            } else {
                $adverts = Advert::whereFkAgent($fk_agent)->paginate($perPage);
            }

        }
        return $adverts;
    }

    public function getMockAdverts(Request $request, AdvertContract $advertRepo)
    {
        $result = $advertRepo->getAdverts($request);
        return $result;
    }
}