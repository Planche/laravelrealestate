<?php

namespace App\Contracts;

use Illuminate\Http\Request;

interface AdvertContract
{
    public function getAdverts(Request $request);

}
