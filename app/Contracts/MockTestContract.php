<?php

namespace App\Contracts;

use Illuminate\Http\Request;

interface MockTestContract
{
    public function getAllAdverts(Request $request);

    public function getMockAdverts(Request $request, AdvertContract $mockTest);

}
