<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Contact extends Notification
{
    use Queueable;

    private $data;
    /**
     * Create a new notification instance.
     *
     * @param  $requestData
     * @return void
     */
    public function __construct($requestData)
    {
        $this->data = $requestData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Realestate Contact')
            ->line('Test Email from laravel.')
            ->line('Sender Full Name - '.$this->data['fullname'])
            ->line('Sender Email - '.$this->data['emailaddress'])
            ->line('Sender Number - '.$this->data['number'])
            ->line('Message body')
            ->line($this->data['message']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
