<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property int $customer_id
 * @property string $name
 * @property-read \App\Models\Customer $customer
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereName($value)
 * @mixin \Eloquent
 */
class Order extends Model
{

    public function customer(){
        return $this->belongsTo('App\Models\Customer');
    }
}
