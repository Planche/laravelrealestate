var elixir = require('laravel-elixir');

elixir(function(mix){
   mix.styles([
       'source/bootstrap/css/bootstrap.css',
       'source/style.css',
       'source/owl-carousel/owl.carousel.css',
       'source/owl-carousel/owl.theme.css',
       'source/owl-carousel/owl.carousel.js',
       'source/slitslider/css/style.css',
       'source/slitslider/css/custom.css'
   ], 'public/css/styles.css');
});