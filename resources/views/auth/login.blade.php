@extends('layouts.main')

@section('content')
    <div class="inside-banner">
        <div class="container">
            <span class="pull-right"><a href="{{ route('main') }}">Home</a> / {{ Route::currentRouteName() }}</span>
            <h2>Login</h2>
        </div>
    </div>
    <!-- banner -->
    <div class="container">
        <div class="spacer">
            <div class="row register">
                <div class="col-lg-6 col-lg-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 ">
                    {!! Form::open(['url' => '/login', 'class' => 'form-inline', 'method' => 'POST']) !!}

                    <div class="form-group-login{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" placeholder="Enter Email" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group-login{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" placeholder="Password" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif

                    </div>

                    <div class="form-group-login">
                        <div class="checkbox">
                            <label for="remember">
                                <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }} style="height: 12px;"> Remember Me
                            </label>
                        </div>
                    </div>

                    <div class="form-group-login">
                        {!! Form::button('Login', ['type' => 'submit','name' => 'submit','class' => 'btn btn-success']) !!}

                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
