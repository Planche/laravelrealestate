<div class="form-notify {{ $errors->has('email') ? 'has-error' : ''}}">
{!! Form::open(['url' => '/notify', 'class' => 'form-inline', 'id' => 'notify', 'method' => 'POST']) !!}
    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter Your email address']) !!}
    <span class="help-block notify">{{ $errors->first('email') }}</span>
    {!! Form::submit('Notify Me!', ['class' => 'btn btn-success']) !!}
{!! Form::close() !!}
</div>
