<div class="pull-right">
    {!! Form::open(['url' => route('main/find'), 'id' => 'sort-form','method' => 'POST']) !!}
    {!! Form::select('sortByPrice', ['' => 'Sort by', 'asc' => 'Price: Low to High', 'desc' => 'Price: High to Low'], null, ['class' => 'form-control', 'id' => 'sort']) !!}
    {!! Form::close() !!}
</div>