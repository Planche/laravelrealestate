<div class="hot-properties hidden-xs">
    <h4>Hot Properties</h4>
    @foreach($sidebar as $side)
        <div class="row">
            <div class="col-lg-4 col-sm-5"><img src="{{ '/storage/uploads/'.$side->id.'/general/small_'.$side->general_image }}"  class="img-responsive img-circle" alt="properties"></div>
            <div class="col-lg-8 col-sm-7">
                <h5><a href="{{ '/main/view-advert/'. $side->id }}">
                        @if($side->bedroom > 1)
                            {{ $side->bedroom . ' Bedrooms and ' }}
                        @else
                            {{ $side->bedroom . ' Bedroom and ' }}
                        @endif

                        @if($side->livingroom > 1)
                            {{ $side->livingroom . ' Living Rooms ' }}
                        @else
                            {{ $side->livingroom . ' Living Room ' }}
                        @endif
                        Aparment on Sale</a>
                </h5>
                <p class="price">${{ number_format($side->price) }}</p> </div>
        </div>
    @endforeach
</div>