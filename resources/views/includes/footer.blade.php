<div class="footer">

    <div class="container">

        <div class="row">
            <div class="col-lg-3 col-sm-3">
                <h4>Information</h4>
                <ul class="row">
                    <li class="col-lg-12 col-sm-12 col-xs-3"><a href="{{ route('about') }}" >About</a></li>
                    <li class="col-lg-12 col-sm-12 col-xs-3"><a href="{{ route('contact') }}" >Contact</a></li>
                </ul>
            </div>

            <div class="col-lg-3 col-sm-3">
                <h4>Newsletter</h4>
                <p>Get notified about the latest properties in our marketplace.</p>
                @include('includes/notify')
            </div>

            <div class="col-lg-3 col-sm-3">
                <h4>Share</h4>
                <a href="https://www.facebook.com/sharer/sharer.php?u=http://bmwclubarmenia.am">
                    <img src="/images/facebook.png"  alt="facebook">
                </a>
                <a href="http://twitter.com/share?url=http://bmwclubarmenia.am">
                    <img src="/images/twitter.png"  alt="twitter">
                </a>
                <a href="http://vk.com/share.php?url=http://bmwclubarmenia.am">
                    <img src="/images/vk.png"  alt="vk">
                </a>
                <a href="#"><img src="/images/instagram.png"  alt="instagram"></a>
            </div>

            <div class="col-lg-3 col-sm-3">
                <h4>Contact us</h4>
                <p><b>www.bmwclubarmenia.am</b><br>
                    <span class="glyphicon glyphicon-map-marker"></span> 4 Charents Street, Armenia <br>
                    <span class="glyphicon glyphicon-envelope"></span> admin@bmwclubarmenia.am<br>
            </div>
        </div>
        <p class="copyright">Copyright 2017. All rights reserved.	</p>
    </div>
</div>