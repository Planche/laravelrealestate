<!-- Header Starts -->
<div class="navbar-wrapper">

    <div class="navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">


                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>


            <!-- Nav Starts -->
            <div class="navbar-collapse  collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li {{ Request::route()->getName() == 'main' ? 'class=active' : '' }}>
                        <a href="{{ route('main') }}" >Home</a>
                    </li>
                    <li {{ Request::route()->getName() == 'about' ? 'class=active' : '' }}>
                        <a href="{{ route('about') }}" >About</a>
                    </li>
                    <li {{ Request::route()->getName() == 'contact' ? 'class=active' : '' }}>
                        <a href="{{ route('contact') }}" >Contact</a>
                    </li>
                </ul>
            </div>
            <!-- #Nav Ends -->

        </div>
    </div>

</div>
<!-- #Header Starts -->

<div class="container">

    <!-- Header Starts -->
    <div class="header">
        <a href="/main" ><img src="{{ '/images/logo.png' }}"  alt="Realestate"></a>

        <ul class="pull-right">
            @if (Auth::check())
                <li><a href="/cabinet/adverts">Cabinet</a> </li>
                <li><a href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form></li>

            @else
                <li><a href="{{ route('register') }}">register</a></li>
                <li><a href="#" data-toggle="modal" data-target="#loginpop">Login</a></li>
            @endif
        </ul>
    </div>
    <!-- #Header Starts -->
</div>