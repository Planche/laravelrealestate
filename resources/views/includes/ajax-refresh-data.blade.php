<!-- properties -->
@foreach($adverts as $adv)
    <div class="col-lg-4 col-sm-6">
        <div class="properties">
            <div class="image-holder"><img src="{{ '/storage/uploads/'.$adv->id.'/general/small_'.$adv->general_image }}"  class="img-responsive" alt="properties">
                <div class="status sold">{{ $adv->sold ? 'sold' : 'new' }}</div>
            </div>
            <h4><a href="{{ '/main/view-advert/'. $adv->id }}">
                    @if($adv->bedroom > 1)
                        {{ $adv->bedroom . ' Bedrooms and ' }}
                    @else
                        {{ $adv->bedroom . ' Bedroom and ' }}
                    @endif

                    @if($adv->livingroom > 1)
                        {{ $adv->livingroom . ' Living Rooms ' }}
                    @else
                        {{ $adv->livingroom . ' Living Room ' }}
                    @endif
                    Aparment on Sale</a>
            </h4>
            <p class="price">Price: ${{ number_format($adv->price) }}</p>
            <div class="listing-detail">
                <span data-toggle="tooltip" data-placement="bottom" data-original-title="BedRoom">{{ $adv->bedroom }}</span>
                <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">{{ $adv->livingroom }}</span>
                <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">{{ $adv->parking }}</span>
                <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">{{ $adv->kitchen }}</span>
            </div>
            <a class="btn btn-primary" href="{{ '/main/view-advert/'. $adv->id }}">View Details</a>
        </div>
    </div>
@endforeach
<!-- properties -->
<div class="col-lg-12 col-sm-12">
    <div class="center">{{ $adverts->links() }}</div>
</div>