@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
    @include('cabinet.sidebar')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Create New Post</div>
                    <div class="panel-body">
                        <a href="{{ url('/cabinet/adverts') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        {{--First fileinput--}}

                        @if(Session::has('success'))
                            <div class="alert-box success">
                                <h2>{!! Session::get('success') !!}</h2>
                            </div>
                        @endif
                        <div class="secure">General Image</div>
                        {!! Form::open(array('url'=>'cabinet/adverts/uploadGeneral', 'method' => 'POST', 'files' => true)) !!}
                        {!! Form::file('file',['accept' => 'image/png, image/jpeg, image/gif', 'id' => 'file', 'class' => 'file file-loading']) !!}

                        {!! Form::close() !!}
                        <br />
                        <br />

                        {{--Second fileinput--}}

                        @if(Session::has('success'))
                            <div class="alert-box success">
                                <h2>{!! Session::get('success') !!}</h2>
                            </div>
                        @endif
                        <div class="secure">Additional Images</div>
                        {!! Form::open(array('url'=>'cabinet/adverts/uploadImages', 'method' => 'POST', 'files' => true)) !!}
                        {!! Form::file('images',['accept' => 'image/png, image/jpeg, image/gif', 'id' => 'images', 'class' => 'file file-loading', 'multiple' => true]) !!}

                        {!! Form::close() !!}
                        <br>
                        <a role="button" class="btn btn-success" href="{{ action('AdvertController@index') }}">Update</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function basename(path) {
            return path.split(/[\\/]/).pop();
        }
        let imagesPreview = {!! json_encode($images) !!};
        let imagesConfig = [];
        if(imagesPreview) {
            for (let i = 0; i < imagesPreview.length; i++) {
                imagesConfig.push({
                    caption: basename(imagesPreview[i]),
                    key: imagesPreview[i]
                });
            }
        } else {imagesPreview = []}
        //console.log(imagesPreview);
        //console.log(imagesConfig);

        // Kartik initial preview General
        $("#file").fileinput({
            uploadUrl:  window.location.origin+"/cabinet/adverts/uploadGeneral",
            uploadAsync: true,
            maxFileCount: 1,
            purifyHtml: true, // this by default purifies HTML data for preview
            dropZoneEnabled: false,
            allowedFileExtensions:  ['jpg', 'png','gif'],
            uploadExtraData: {_token: $('meta[name="csrf-token"]').attr('content')},
            overwriteInitial: false,
            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
            initialPreview: [{!! json_encode($image) !!}],
            initialPreviewShowDelete: true,
            initialPreviewConfig: [{key: {!! json_encode($image) !!}}],
            deleteExtraData: {_token: $('meta[name="csrf-token"]').attr('content')},
            deleteUrl: "/cabinet/adverts/deleteImages"
        });
        //Kartik initial preview Additional
        $("#images").fileinput({
            uploadUrl: window.location.origin+"/cabinet/adverts/uploadImages",
            uploadAsync: true,
            maxFileCount: 10,
            purifyHtml: true, // this by default purifies HTML data for preview
            dropZoneEnabled: false,
            overwriteInitial: false,
            allowedFileExtensions:  ['jpg', 'png','gif'],
            uploadExtraData: {_token: $('meta[name="csrf-token"]').attr('content')},
            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
            initialPreview: imagesPreview,
            initialPreviewShowDelete: true,
            initialPreviewConfig: imagesConfig

                //[{caption: "cat.jpg", key:"cat.jpg"},{key:"wolf.jpg"}]
                //{key: "cat.jpg"}
                //{key: "wolf.jpg"}
            ,
            //fileActionSettings: [{showUpload: true}],
            deleteExtraData: {_token: $('meta[name="csrf-token"]').attr('content')},
            deleteUrl: "/cabinet/adverts/deleteImages"

        });
        alert("Please disable Adblock before uploading images");
    </script>
@endsection