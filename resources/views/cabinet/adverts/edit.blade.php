@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('cabinet.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Post #{{ $advert->id }}</div>
                    <div class="panel-body">
                        <a href="{{ url('/cabinet/adverts') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($advert, [
                            'method' => 'PATCH',
                            'url' => ['/cabinet/adverts', $advert->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('cabinet.adverts.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset('source/addMaps.js')}}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvVXYNdc8lLCWhd7X5RbiQalwN0Rck48s"></script>
@endpush
