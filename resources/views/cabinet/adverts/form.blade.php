<div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Price', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('price', null, ['class' => 'form-control']) !!}
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', 'Address', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('address', null, ['class' => 'form-control']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('location', 'Location', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div id="map_canvas" style="width:100%; height:280px"></div>
    </div>
</div>

<div class="form-group {{ $errors->has('bedroom') ? 'has-error' : ''}}">
    {!! Form::label('bedroom', 'Bedroom', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('bedroom', null, ['class' => 'form-control']) !!}
        {!! $errors->first('bedroom', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('livingroom') ? 'has-error' : ''}}">
    {!! Form::label('livingroom', 'Livingroom', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('livingroom', null, ['class' => 'form-control']) !!}
        {!! $errors->first('livingroom', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('parking') ? 'has-error' : ''}}">
    {!! Form::label('parking', 'Parking', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('parking', null, ['class' => 'form-control']) !!}
        {!! $errors->first('parking', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('kitchen') ? 'has-error' : ''}}">
    {!! Form::label('kitchen', 'Kitchen', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('kitchen', null, ['class' => 'form-control']) !!}
        {!! $errors->first('kitchen', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('hot') ? 'has-error' : ''}}">
    {!! Form::label('hot', 'Hot', ['class' => 'col-md-4 control-label', 'id' => 'radiopad']) !!}
    <div class="col-md-6">
        {!! Form::label('hot', 'Yes&nbsp;') !!}
        {!! Form::radio('hot', '1', false, ['class' => 'radio-inline']) !!}
        {!! Form::label('hot', '&nbsp;No&nbsp;') !!}
        {!! Form::radio('hot', '0', false, ['class' => 'radio-inline']) !!}
        {!! $errors->first('hot', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('sold') ? 'has-error' : ''}}">
    {!! Form::label('sold', 'Sold', ['class' => 'col-md-4 control-label', 'id' => 'radiopad']) !!}
    <div class="col-md-6">
        {!! Form::label('sold', 'Yes&nbsp;') !!}
        {!! Form::radio('sold', '1', false, ['class' => 'radio-inline']) !!}
        {!! Form::label('sold', '&nbsp;No&nbsp;') !!}
        {!! Form::radio('sold', '0', false, ['class' => 'radio-inline']) !!}
        {!! $errors->first('sold', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    {!! Form::label('type', 'Type', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('type', ['Apartment', 'Building', 'Office Space'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('recommend') ? 'has-error' : ''}}">
    {!! Form::label('recommend', 'Recommend', ['class' => 'col-md-4 control-label', 'id' => 'radiopad']) !!}
    <div class="col-md-6">
        {!! Form::label('recommend', 'Yes&nbsp;') !!}
        {!! Form::radio('recommend', '1', false, ['class' => 'radio-inline']) !!}
        {!! Form::label('recommend', '&nbsp;No&nbsp;') !!}
        {!! Form::radio('recommend', '0', false, ['class' => 'radio-inline']) !!}
        {!! $errors->first('recommend', '<p class="help-block">:message</p>') !!}
    </div>
</div>
{!! Form::hidden('location', null, ['id' => 'location']) !!}
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
