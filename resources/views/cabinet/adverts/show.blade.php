@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('cabinet.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Post {{ $advert->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/cabinet/adverts') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/cabinet/adverts/' . $advert->id . '/edit') }}" title="Edit Post"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['cabinet/adverts', $advert->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Post',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th> ID </th><td>{{ $advert->id }}</td>
                                    </tr>
                                    <tr><th> Price </th><td> {{ $advert->price }} </td></tr>
                                    <tr><th> Address </th><td> {{ $advert->address }} </td></tr>
                                    <tr><th> Fk_agent </th><td> {{ $advert->fk_agent }} </td></tr>
                                    <tr><th> Bedrooms </th><td> {{ $advert->bedroom }} </td></tr>
                                    <tr><th> Livingrooms </th><td> {{ $advert->livingroom }} </td></tr>
                                    <tr><th> Parking </th><td> {{ $advert->parking }} </td></tr>
                                    <tr><th> Kitchen </th><td> {{ $advert->kitchen }} </td></tr>
                                    <tr><th> General_image </th><td> {{ $advert->general_image }} </td></tr>
                                    <tr><th> Description </th><td> {{ $advert->description }} </td></tr>
                                    <tr><th> Location </th><td> {{ $advert->location }} </td></tr>
                                    <tr><th> Hot </th><td> {{ $advert->hot }} </td></tr>
                                    <tr><th> Sold </th><td> {{ $advert->sold }} </td></tr>
                                    <tr><th> Type </th><td> {{ $advert->type }} </td></tr>
                                    <tr><th> Recommend </th><td> {{ $advert->recommend }} </td></tr>
                                    <tr><th> Created_at </th><td> {{ $advert->created_at }} </td></tr>
                                    <tr><th> Updated_at </th><td> {{ $advert->updated_at }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
