@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('cabinet.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Admin #{{ $admin->id }}</div>
                    <div class="panel-body">
                        <a href="{{ url('/cabinet/admins') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($admin, [
                            'method' => 'PATCH',
                            'url' => ['/cabinet/admins', $admin->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('cabinet.admins.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
