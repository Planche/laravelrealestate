<div class="col-md-3">
    <div class="panel panel-default panel-flush">
        <div class="panel-heading">
            Sidebar
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                @if(Auth::id() == 1)
                <li role="presentation">
                    <a href="{{ url('/cabinet/admins') }}">
                        Admins
                    </a>
                </li>
                @endif
                <li role="presentation">
                    <a href="{{ url('/cabinet/adverts') }}">
                        Adverts
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
