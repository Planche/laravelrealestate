@extends('layouts.main')

@section('content')
    <div class="inside-banner">
        <div class="container">
            <span class="pull-right"><a href="{{ route('main') }}" >Home</a> / Buy</span>
            <h2>Buy</h2>
        </div>
    </div>
    <div class="col-lg-3 col-sm-4 hidden-xs">

    </div>
    <div class="container">
        <div class="properties-listing spacer">

            <div class="row">
                <div class="col-lg-3 col-sm-4 hidden-xs">
                    @include('includes.hot')
                </div>
                <div class="col-lg-9 col-sm-8 ">

                    <h2>@if($advert->bedroom > 1)
                            {{ $advert->bedroom . ' Bedrooms and ' }}
                        @else
                            {{ $advert->bedroom . ' Bedroom and ' }}
                        @endif

                        @if($advert->livingroom > 1)
                            {{ $advert->livingroom . ' Living Rooms ' }}
                        @else
                            {{ $advert->livingroom . ' Living Room ' }}
                        @endif
                    </h2>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="property-images">
                                <!-- Slider Starts -->
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators hidden-xs">
                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                        @isset($images)
                                        @foreach(range(1,count($images)) as $s)
                                        <li data-target="#myCarousel" data-slide-to="{{ $s }}" class=""></li>
                                        @endforeach
                                        @endisset
                                    </ol>
                                    <div class="carousel-inner">
                                        <!-- Item 1 -->
                                        <div class="item active">
                                            <img src="{{ '/storage/uploads/'. $advert->id . '/general/small_' . $advert->general_image }}"  class="properties" alt="properties" />
                                        </div>
                                        <!-- #Item 1 -->
                                        @isset($images)
                                        @foreach($images as $image)
                                        <!-- Item 2 -->
                                        <div class="item">
                                            <img src="{{ $image }}"  class="properties" alt="properties" />
                                        </div>
                                        @endforeach
                                        @endisset
                                        <!-- #Item 2 -->
                                    </div>
                                    @isset($images)
                                    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                                    <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                    @endisset
                                </div>
                                <!-- #Slider Ends -->

                            </div>




                            <div class="spacer"><h4><span class="glyphicon glyphicon-th-list"></span> Properties Detail</h4>
                                <p>{{ $advert->description }}</p>
                            </div>
                            <div><h4><span class="glyphicon glyphicon-map-marker"></span> Location</h4>
                                <div class="well"><div id="map_canvas" style="width:100%; height:280px"></div></div>
                            </div>

                        </div>
                        <div class="col-lg-4">
                            <div class="col-lg-12  col-sm-6">
                                <div class="property-info">
                                    <p class="price">$ {{ number_format($advert->price) }}</p>
                                    <p class="area"><span class="glyphicon glyphicon-map-marker"></span>{{ $advert->address }}</p>
                                    {{ Form::hidden('location',$advert->location) }}
                                    <div class="profile">
                                        <span class="glyphicon glyphicon-user"></span> Agent Details
                                        <p>{{ $advert->user->name }}</p>
                                    </div>
                                </div>

                                <h6><span class="glyphicon glyphicon-home"></span> Availabilty</h6>
                                <div class="listing-detail">
                                    <span data-toggle="tooltip" data-placement="bottom" data-original-title="BedRoom">{{ $advert->bedroom }}</span>
                                    <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">{{ $advert->livingroom }}</span>
                                    <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">{{ $advert->parking }}</span>
                                    <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">{{ $advert->kitchen }}</span>
                                </div>

                            </div>
                            <div class="col-lg-12 col-sm-6 ">
                                <div class="enquiry">
                                    <h6><span class="glyphicon glyphicon-envelope"></span> Post Enquiry</h6>
                                    {!! Form::open(['url' => '/main/message', 'class' => 'form-inline', 'id' => 'message-form', 'method' => 'POST']) !!}
                                        <div class="email-form-group">
                                            {!! Form::text('mail', null, ['class' => 'form-control', 'placeholder' => 'you@yourdomain.com']) !!}
                                            <span class="help-block email">{{ $errors->first('email') }}</span>
                                        </div>
                                        <div class="message-form-group">
                                            {!! Form::textarea('message',null,['class' => 'form-control', 'placeholder' => 'Whats on your mind']) !!}
                                            <span class="help-block message">{{ $errors->first('message') }}</span>
                                        </div>
                                        {!! Form::submit('Send Message!', ['class' => 'btn btn-success']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset('source/showMaps.js')}}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvVXYNdc8lLCWhd7X5RbiQalwN0Rck48s"></script>
@endpush