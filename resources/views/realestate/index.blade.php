@extends('layouts.main')

@section('content')
<div class="">

    <div id="slider" class="sl-slider-wrapper">

        <div class="sl-slider">

            @foreach($adverts as $adv)
            <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                <div class="sl-slide-inner">
                    <div class="bg-img" style="background-image: url('{{ '/storage/uploads/'. $adv->id . '/general/' . $adv->general_image }}')"></div>
                    <h2><a href="{{ '/main/view-advert/'. $adv->id }}">@if($adv->bedroom > 1)
                                        {{ $adv->bedroom . ' Bedrooms and ' }}
                                    @else
                                        {{ $adv->bedroom . ' Bedroom and ' }}
                                    @endif

                                    @if($adv->livingroom > 1)
                                        {{ $adv->livingroom . ' Living Rooms ' }}
                                    @else
                                        {{ $adv->livingroom . ' Living Room ' }}
                                    @endif
                            Aparment on Sale</a>
                    </h2>
                    <blockquote>
                        <p class="location"><span class="glyphicon glyphicon-map-marker"></span>{{ ' '.$adv->address }}</p>
                        <p>{{ str_limit($adv->description,50,'...') }}</p>
                        <cite>$ {{ number_format($adv->price) }}</cite>
                    </blockquote>
                </div>
            </div>
            @endforeach
        </div><!-- /sl-slider -->



        <nav id="nav-dots" class="nav-dots">
            @if($adverts_count >= 1)
            <span class="nav-dot-current"></span>
            @endif
            @foreach(range(2,$adverts_count) as $line)
                <span></span>
            @endforeach
        </nav>

    </div><!-- /slider-wrapper -->
</div>



<div class="banner-search">
    <div class="container">
        <!-- banner -->
        <h3>Buy, Sale & Rent</h3>
        <div class="searchbar">
            <div class="row">

                {!! Form::open(['url' => 'main/find', 'method' => 'GET']) !!}
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        {!! Form::text('advert', null, ['class' => 'form-control','placeholder' => 'Search of Properties']) !!}
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 ">
                            {!! Form::select('apartment', ['' => 'Property', 'Apartment', 'Building', 'Office'], null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="col-lg-3 col-sm-4">
                            {!! Form::select('price', ['' => 'Price',
                                '150000-200000' => '$150,000 - $200,000',
                                '200000-250000' => '$200,000 - $250,000',
                                '250000-300000' =>'$250,000 - $300,000',
                                '300000' => '$300,000 - above'], null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="col-lg-3 col-sm-4">
                            {!! Form::submit('Find',['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}

                @if(Auth::guest())
                <div class="col-lg-5 col-lg-offset-1 col-sm-6 ">
                    <p>Join now and get updated with all the properties deals.</p>
                    <button class="btn btn-info" data-toggle="modal" data-target="#loginpop">Login</button>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- banner -->
<div class="container">
    <div class="properties-listing spacer"> <a href="{{ route('main/find') }}"  class="pull-right viewall">View All Listing</a>
        <h2>Featured Properties</h2>
        <div id="owl-example" class="owl-carousel">

            @foreach($featured as $feat)
            <div class="properties">
                <div class="image-holder"><img src="{{ '/storage/uploads/'. $feat->id . '/general/small_' . $feat->general_image }}"  class="img-responsive" alt="properties"/>
                    <div class="status sold">{{ $feat->sold ? 'sold' : 'new' }}</div>
                </div>
                <h4><a href="{{ '/main/view-advert/'. $feat->id }}" >
                        @if($feat->bedroom > 1)
                            {{ $feat->bedroom . ' Bedrooms and ' }}
                        @else
                            {{ $feat->bedroom . ' Bedroom and ' }}
                        @endif

                        @if($feat->livingroom > 1)
                            {{ $feat->livingroom . ' Living Rooms ' }}
                        @else
                            {{ $feat->livingroom . ' Living Room ' }}
                        @endif
                        Aparment on Sale</a>
                </h4>
                <p class="price">Price: ${{ number_format($feat->price) }}</p>
                <div class="listing-detail">
                    <span data-toggle="tooltip" data-placement="bottom" data-original-title="Bedroom">{{ $feat->bedroom }}</span>
                    <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">{{ $feat->livingroom }}</span>
                    <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">{{ $feat->parking }}</span>
                    <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">{{ $feat->kitchen }}</span>
                </div>
                <a class="btn btn-primary" href="{{ '/main/view-advert/'. $feat->id }}" >View Details</a>
            </div>
            @endforeach

        </div>
    </div>
    <div class="spacer">
        <div class="row">
            <div class="col-lg-6 col-sm-9 recent-view">
                <h3>About Us</h3>
                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.<br>
                    <a href="{{ route('about') }}" >Learn More</a>
                </p>

            </div>
            <div class="col-lg-5 col-lg-offset-1 col-sm-3 recommended">
                <h3>Recommended Properties</h3>
                <div id="myCarousel" class="carousel slide">
                    <ol class="carousel-indicators">
                        @if($recommend_count >= 1)
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        @endif

                        @if($recommend_count > 1)
                            @foreach(range(1,$recommend_count-1) as $count)
                        <li data-target="#myCarousel" data-slide-to="{{ $count }}"></li>
                            @endforeach
                        @endif

                    </ol>
                    <!-- Carousel items -->
                    <div class="carousel-inner">

                        @foreach($recommend as $key => $rec)
                        <div class="item {{ $loop->first ? 'active' : '' }}">
                            <div class="row">
                                <div class="col-lg-4"><img src="{{ '/storage/uploads/'. $rec->id . '/general/small_' . $rec->general_image }}"  class="img-responsive" alt="properties"/></div>
                                <div class="col-lg-8">
                                    <h5><a href="{{ '/main/view-advert/'. $rec->id }}" >@if($rec->bedroom > 1)
                                                {{ $rec->bedroom . ' Bedrooms and ' }}
                                            @else
                                                {{ $rec->bedroom . ' Bedroom and ' }}
                                            @endif

                                            @if($rec->livingroom > 1)
                                                {{ $rec->livingroom . ' Living Rooms ' }}
                                            @else
                                                {{ $rec->livingroom . ' Living Room ' }}
                                            @endif
                                            Aparment on Sale</a></h5>
                                    <p class="price">${{ number_format($rec->price) }}</p>
                                    <a href="{{ '/main/view-advert/'. $rec->id }}" class="more">More Detail</a> </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
