@extends('layouts.main')

@section('content')
<div class="inside-banner">
    <div class="container">
        <span class="pull-right"><a href="{{ route('main') }}">Home</a> / Contact Us</span>
        <h2>Contact Us</h2>
    </div>
</div>
<!-- banner -->

{{ Auth::id() }}
<div class="container">
    <div class="spacer">
        <div class="row contact">
            <div class="col-lg-6 col-sm-6 ">
                <div class="contact-form">
                    {!! Form::open(['url' => '/contact', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                    <div class="group-form {{ $errors->has('fullname') ? 'has-error' : ''}}">
                        {!! Form::text('fullname', null, ['class' => 'form-control','placeholder' => 'Full Name']) !!}
                        {!! $errors->first('fullname', '<strong class="help-block">:message</strong>') !!}
                    </div>
                    <div class="group-form {{ $errors->has('emailaddress') ? 'has-error' : ''}}">
                        {!! Form::text('emailaddress', null, ['class' => 'form-control','placeholder' => 'Email Address']) !!}
                        {!! $errors->first('emailaddress', '<strong class="help-block">:message</strong>') !!}
                    </div>
                    <div class="group-form {{ $errors->has('number') ? 'has-error' : ''}}">
                        {!! Form::text('number', null, ['class' => 'form-control','placeholder' => 'Contact Number(Optional)']) !!}
                        {!! $errors->first('number', '<strong class="help-block">:message</strong>') !!}
                    </div>
                    <div class="group-form {{ $errors->has('message') ? 'has-error' : ''}}">
                        {!! Form::textarea('message', null, ['class' => 'form-control','placeholder' => 'Message']) !!}
                        {!! $errors->first('message', '<strong class="help-block">:message</strong>') !!}
                    </div>
                    <div class="group-form">
                        {!! Form::button('Send Massage', ['type' => 'submit','name' => 'submit','class' => 'btn btn-success']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <br>
                @if(Session::has('success_message'))
                    <div class="alert alert-success">
                        {{ Session::get('success_message') }}
                        <div class="pull-right">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
                @if(Session::has('error_message'))
                    <div class="alert alert-danger">
                        {{ Session::get('error_message') }}
                        <div class="pull-right">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                    </div>
                @endif
            </div>
            <div class="col-lg-6 col-sm-6 ">
                <div class="well">
                    <div id="map_canvas" style="width:100%; height:300px"></div>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('source/showMaps.js')}}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js"></script>
@endpush
