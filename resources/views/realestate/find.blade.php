@extends('layouts.main')

@section('content')
    <!-- banner -->
    <div class="inside-banner">
        <div class="container">
            <span class="pull-right"><a href="{{ route('main') }}" >Home</a> / Buy, Sale & Rent</span>
            <h2>Buy, Sale & Rent</h2>
        </div>
    </div>
    <!-- banner -->
<div class="container">
    <div class="properties-listing spacer">

        <div class="row">
            <div class="col-lg-3 col-sm-4 ">
                {!! Form::open(['url' => 'main/find', 'method' => 'GET']) !!}
                <div class="search-form"><h4><span class="glyphicon glyphicon-search"></span> Search for</h4>
                    {!! Form::text('advert', null, ['class' => 'form-control','placeholder' => 'Search of Properties']) !!}
                    <div class="row">
                        <div class="col-lg-6">
                            {!! Form::select('apartment', ['' => 'Property', 'Apartment', 'Building', 'Office'], null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="col-lg-6">
                            {!! Form::select('price', ['' => 'Price',
                                '150000-200000' => '$150,000 - $200,000',
                                '200000-250000' => '$200,000 - $250,000',
                                '250000-300000' =>'$250,000 - $300,000',
                                '300000' => '$300,000 - above'], null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    {!! Form::submit('Find',['class' => 'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}

                @include('includes.hot')


            </div>

            <div class="col-lg-9 col-sm-8">
                <div class="sortby clearfix">
                    <div class="pull-left result">
                        Showing: @if($advertsCount < 3)
                                     {{ $advertsCount }} of {{ $advertsCount }}
                                 @else
                                     3 of {{ $advertsCount }}
                                 @endif
                    </div>
                    @include('includes.sort')

                </div>
                <div class="row" id="advert-data">

                    @include('includes.ajax-refresh-data')

                </div>
            </div>
        </div>
    </div>
</div>
@endsection