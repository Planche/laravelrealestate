<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <title>Document</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="images/favicon.png" type="image/png" />
    <!-- Styles -->

    <link href="{{ asset('source/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('source/style.css') }}" rel="stylesheet">
    <script src="{{ asset('source/js/jquery-1.9.1.min.js') }}" ></script>
    <script src="{{ asset('source/bootstrap/js/bootstrap.js') }}" ></script>
    <script src="{{ asset('source/script.js') }}" ></script>
    <!-- Owl stylesheet -->
    <link href="{{ asset('source/owl-carousel/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('source/owl-carousel/owl.theme.css') }}" rel="stylesheet">
    <script src="{{ asset('source/owl-carousel/owl.carousel.js') }}" ></script>
    <!-- Owl stylesheet -->

    <!-- slitslider -->
    <link href="{{ asset('source/slitslider/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('source/slitslider/css/custom.css') }}" rel="stylesheet">
    <script src="{{ asset('source/slitslider/js/modernizr.custom.79639.js') }}" ></script>
    <script src="{{ asset('source/slitslider/js/jquery.ba-cond.min.js') }}" ></script>
    <script src="{{ asset('source/slitslider/js/jquery.slitslider.js') }}" ></script>
    <!-- slitslider -->
    @stack('scripts')
    {{--@stack('showscripts')--}}


</head>
<body>
@include('includes.header')

@yield('content')

@include('includes.footer')
@include('includes.loginmodal')
<script src="{{ asset('source/ajax.js') }}" ></script>
</body>
</html>