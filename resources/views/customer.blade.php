<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Customer</title>
</head>
<body>

<h1>{{ $customer->name }}</h1>

<ul>
    @foreach($customer->orders as $order)
    <li>{{ $order->name }}</li>
    @endforeach
    </ul>

</body>
</html>