<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Advert::class, function (Faker\Generator $faker) {
    return [
        'price'         => $faker->randomNumber(1),
        'address'       => $faker->address,
        'fk_agent'      => $faker->randomNumber(1),
        'bedroom'       => $faker->randomNumber(1),
        'livingroom'    => $faker->randomNumber(1),
        'parking'       => $faker->randomNumber(1),
        'kitchen'       => $faker->randomNumber(1),
        'general_image' => $faker->imageUrl(),
        'description'   => $faker->text,
        'location'      => $faker->randomNumber(8),
        'hot'           => $faker->boolean(),
        'sold'          => $faker->boolean(),
        'type'          => $faker->boolean(),
        'recommend'     => $faker->boolean(),
    ];
});
