<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumInteger('price');
            $table->string('address','255');
            $table->mediumInteger('fk_agent');
            $table->smallInteger('bedroom');
            $table->smallInteger('livingroom');
            $table->smallInteger('parking');
            $table->smallInteger('kitchen');
            $table->string('general_image','200');
            $table->text('description');
            $table->string('location','50');
            $table->smallInteger('hot');
            $table->smallInteger('sold');
            $table->string('type','50');
            $table->smallInteger('recommend');
            $table->timestamps();
            $table->engine = 'innodb';
            $table->charset = 'UTF8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adverts');
    }
}
