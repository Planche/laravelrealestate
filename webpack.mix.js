const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
mix.styles([
    'public/source/bootstrap/css/bootstrap.css',
    'public/source/style.css',
    'public/source/owl-carousel/owl.carousel.css',
    'public/source/owl-carousel/owl.theme.css',
    'public/source/owl-carousel/owl.carousel.js',
    'public/source/slitslider/css/style.css',
    'public/source/slitslider/css/custom.css'
], 'public/css/styles.css');
mix.js([
    'public/source/js/jquery-1.9.1.min.js',
    'public/source/bootstrap/js/bootstrap.js',
    'public/source/script.js'
], 'public/js/scripts.js');
mix.js([
    'public/source/owl-carousel/owl.carousel.js',
    'public/source/slitslider/js/modernizr.custom.79639.js',
    'public/source/slitslider/js/jquery.ba-cond.min.js',
    'public/source/slitslider/js/jquery.slitslider.js'
], 'public/js/sliders.js');