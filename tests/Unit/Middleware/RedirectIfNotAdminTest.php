<?php

namespace Tests\Unit;

use App\Http\Middleware\AdminMiddleware;
use App\Models\User;
use Tests\TestCase;

class RedirectIfNotAdminTest extends TestCase
{

    protected $user;

    public function testRedirectIfAdmin()
    {
        $this->loginDifferentUsers(1);
        $request = request()->create('http://laravel.loc/cabinet/admins', 'GET');

        $middleware = new AdminMiddleware($this->user);
        $response   = $middleware->handle($request, function (){});
        $this->assertEquals($response, null);
    }

    public function testRedirectIfNotAdmin()
    {
        $this->loginDifferentUsers(2);
        $request = request()->create('http://laravel.loc/cabinet/admins', 'GET');

        $middleware = new AdminMiddleware($this->user);
        $response   = $middleware->handle($request, function (){});

        $this->assertEquals($response->getTargetUrl(), route('login'));
    }

    protected function loginDifferentUsers($id)
    {
        $this->user = User::find($id);
        $this->be($this->user);
        $this->user = auth()->guard();
    }
}