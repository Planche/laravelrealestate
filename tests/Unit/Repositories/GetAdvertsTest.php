<?php

namespace Tests\Unit;

use App\Contracts\AdvertContract;
use App\Contracts\MockTestContract;
use App\Models\Advert;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Mockery\Mock;
use Tests\TestCase;

class GetAdvertsTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;

    public function testGetAllWithoutKeyword()
    {
        /*$this->loginDifferentUsers(1);
        $request = request();
        $advert = factory(Advert::class)->create();
        $advertRepo = $this->app->make(AdvertContract::class);
        $result = $advertRepo->getAdverts($request);
        $this->assertEquals($advert->price, $result[0]->price);
        $this->assertEquals($advert->address, $result[0]->address);
        $this->assertEquals($advert->location, $result[0]->location);*/
    }

    public function testMockAdverts()
    {
        $this->loginDifferentUsers(1);
        $request = request();
        $advert = factory(Advert::class)->create();
        $mockTestRepo = $this->app->make(MockTestContract::class);
        $mockAdvertContract = \Mockery::mock(AdvertContract::class);
        $mockAdvertContract->shouldReceive('getAdverts')->andReturn([$advert])->getMock();
        $result = $mockTestRepo->getMockAdverts($request, $mockAdvertContract);

        $this->assertEquals($advert->price, $result[0]->price);

    }
    protected function loginDifferentUsers($id)
    {
        $this->user = User::find($id);
        $this->be($this->user);
        $this->user = auth()->guard();
    }


}